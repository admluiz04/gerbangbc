=== Post Sliders & Post Grids ===

Contributors:nik00726
Donate link:http://www.i13websolution.com/donate-wordpress_image_thumbnail.php
Tags:Post Grid,Custom Post Grid,post Grid Display,latest post slider,advance post slider,post thumbnail slider,responsive recent post slider,recent post slider,responsive post slider,post carousel,responsive posts carousel,post carousel widget,post thumbnail slider
Requires at least:3.5
Tested up to:4.9
Version:1.0.2
Stable tag:1.0.2
License:GPLv2 or later
License URI:http://www.gnu.org/licenses/gpl-2.0.html

Post Slider & Grid is beautiful responsive post thumbnail image slider and also support post grid display.It support post exclusion,Categort exclusion and also support custom post type.


== Description ==

Post Sliders & Grids is beautiful responsive post thumbnail image slider as well as post grid plugin.It support post exclusion,Categort exclusion and also support custom post type.Admin can easly create responsive post slider or post grid within minute.Admin can exclude post type,categories and post.Admin can also specify sorting by date,title,author,name,random,comment count etc.Admin can change thumbnail height width also admin can change settings to show/hide pager caption etc.


**Find WordPress Post Sliders & Post Grids Pro Plugin(Unlimited Slider+Vertical Post Slider + Post Grids) at [Post Sliders & Grids](https://www.i13websolution.com/wordpress-post-slider-carousel-pro.html)**

**[Live Demo WordPress Post Grids](http://blog.i13websolution.com/wordpress-post-grid-live-preview/)**

**[Live Demo WordPress Vertical Post Slider Pro](http://blog.i13websolution.com/live-preview-vertical-post-slider/)**

**[Live Demo WordPress Horizontal Post Slider Pro](http://blog.i13websolution.com/live-preview-horizontal-post-slider/)**


**[Live Demo WordPress Latest Post Slider Pro](http://blog.i13websolution.com/live-preview-latest-post-slider/)**

**Post Slider Video**

[youtube https://www.youtube.com/watch?v=YCkhxBkALck]


**Post Grid Video**

[youtube https://www.youtube.com/watch?v=VTiw3IhZgtg]



**Please rate this plugin if you find it useful**


**=Features Post Slider=**


1. Categort exclusion.

2. Post exclusion.

3. Custom post type exclusion.

4. Support Max number of post.

5. Preview your circle slider before use it.

6. Support sort by.

7. changes to images height,width

8. Support Caption

9. Admin can set slider as slide with arrow left and right arrow.

10. Create Post grid easly.



**=Post Slider Pro Features(Add On)=**


1. Unlimited Slider(Multiple sliders).

2. Support Horizontal and Vertical Post Slider Carousel

3. Support Horizontal Ticker(continuous) Post Carousel Slider

4. Support Vertical Ticker(continuous) Post Carousel Slider

5. 16 easing effects

6. You can use as recent(Latest) Post Slider



**=Features Post Grids=**


1. Categort exclusion.

2. Post exclusion.

3. Custom post type exclusion.

4. Support Max number of post.

5. Preview your Post Grid before use.

6. Support sort by.

10. Create Post grid easly.



**=Post Grids Pro Features(Add On)=**

1. Unlimited Post Grids.

2. Ajax pagination in grid

3. Social sharing in post grids


[Get Support](https://www.i13websolution.com/contacts)


== Installation ==


This plugin is easy to install like other plug-ins of Wordpress as you need to just follow the below mentioned steps:

1. upload post-slider-carousel/ folder to wp-Content/plugins folder.

2. Activate the plugin from Dashboard / Plugins window.

4. Now Plugin is Activated, Go to the Usage section to see how to use Circle Slider plus Lightbox.

### Usage ###

1.Use of Post Slider/Post Grid  easly after activating plugin go to post sliders and Grid.

2.You can set desired settings.

3.You can preview slider/grid.

4.You can add this slider to your wordpress page/post by adding this shortcode to [psc_print_post_slider_carousel]  OR you can add this to 
  your theme   by adding this code echo do_shortcode('[psc_print_post_slider_carousel]'); to your theme

5.You can add this post grid to your wordpress page/post by adding this shortcode to [psc_print_post_grid]   OR you can add this to 
  your theme   by adding this code echo do_shortcode('[psc_print_post_grid] '); to your theme


== Screenshots ==

1. Slider Settings
2. Slider Preview
3. Pro Version Manage Sliders
4. Pro Version Slider Settings
5. Pro Version Slider Preview
6. Post Grid Preview
7. Post Grid Settings
8. Pro version unlimited post grids


== License ==

This plugin is free for everyone! Since it's released under the GPL, you can use it free of charge on your personal or commercial blog. But you can make some donations if you realy find it useful.


== Changelog ==

= 1.0.2 =

* Added new feature post grid, You can now create post grid easly.
* plgin is now translatable.


= 1.0 =

* Fixed for shorcode not working in wordpress 4.8 widgets

= 1.0 =

* Stable 1.0 first release


== Upgrade notice ==


= 1.0 =

* Stable 1.0 first release


== Frequently asked questions ==

1.How to use ?

For More info use readme installation and usage notes.
