<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('WP_MEMORY_LIMIT', '900M');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'gerbangh_website_v2');

/** MySQL database username */
define('DB_USER', 'gerbangh_user');

/** MySQL database password */
define('DB_PASSWORD', 'ydz7caaRyy7P');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'q@YB#4@8ge0/+)eD<f#fCBC+-9>6o@}T<iKko2.2K]!:sQph5Qa6~!EphL}PhMc*');
define('SECURE_AUTH_KEY',  'D!UL6#ZBi[9;Gz>^FAPZ-a-17Inc67HcN!bqN6=YOL`:jV=kn~XVyFF.9|zGC(I~');
define('LOGGED_IN_KEY',    ',fK#[-lc{IMgu!f.Rd)>45|5>QQGTod~i~.30<*,{(VFR1BfcF4hB2=J`:A.Jg.w');
define('NONCE_KEY',        '?2CDN!Q^76y?y^4g/`(Z&1x Vn6AKWbI}73P/Awx1;DNDJrUk5%#5xpVnj$>BvHH');
define('AUTH_SALT',        '7YmnfUj)y*+G.f[KT7/4KuRqz$36ZGG]YU,)tpUZj76<o+.HRF#*C)$ZA`XfR{`+');
define('SECURE_AUTH_SALT', 'h<{LN+exZWX54?>{Yho~FMCD5Iu7X{w)By`0k2;)wvm5mF&3/i k1 px1M_zs!-p');
define('LOGGED_IN_SALT',   '+RiPoW)%N)4k7d`BpidE{}/OAJurz=9d=}:fKd|Py%XN!D:*P-TABu<*DGbw(@EM');
define('NONCE_SALT',       'QXM+?Nc^7T2&LyO4F1[R+E@144 7*PsMBu^abU1m=uNTv0gNzr IhnHl[-=;l)37');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('FORCE_SSL_LOGIN', false);