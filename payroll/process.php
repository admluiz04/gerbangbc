<?php

//print_r($_POST);

$companyname = filter_var($_POST['companyname'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$email = filter_var($_POST['email'],FILTER_SANITIZE_EMAIL);
$firstname = filter_var($_POST['firstname'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$lastname = filter_var($_POST['lastname'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$websitename = filter_var($_POST['websitename'],FILTER_SANITIZE_URL);
$phone = filter_var($_POST['phone'],FILTER_SANITIZE_NUMBER_FLOAT);
$message = filter_var($_POST['message'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);

$to = 'salespayroll@gerbangholdings.com';
$subject = 'New Payroll Enquiry from '.$companyname;

$content = '<head>
<style>
* {
  margin: 0;
  padding: 0;
  font-size: 100%;
  font-family: \'Avenir Next\', "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
  line-height: 1.65; }

img {
  max-width: 100%;
  margin: 0 auto;
  display: block; }

body,
.body-wrap {
  width: 100% !important;
  height: 100%;
  background: #f8f8f8;
  -webkit-font-smoothing: antialiased;
  -webkit-text-size-adjust: none; }

a {
  color: #ff5858;
  text-decoration: none; }

.text-center {
  text-align: center; }

.text-right {
  text-align: right; }

.text-left {
  text-align: left; }

.button {
  display: inline-block;
  color: white;
  background:  rgb(79, 182, 206);
  border: solid  rgb(79, 182, 206);
  border-width: 10px 20px 8px;
  font-weight: bold;
  border-radius: 4px; }

h1, h2, h3, h4, h5, h6 {
  margin-bottom: 20px;
  line-height: 1.25; }

h1 {
  font-size: 32px; }

h2 {
  font-size: 28px; }

h3 {
  font-size: 24px; }

h4 {
  font-size: 20px; }

h5 {
  font-size: 16px; }

p, ul, ol {
  font-size: 16px;
  font-weight: normal;
  margin-bottom: 20px; }

.container {
  display: block !important;
  clear: both !important;
  margin: 0 auto !important;
  max-width: 580px !important; }

.container table {
	width: 100% !important;
	border-collapse: collapse; }

.container .masthead {
	padding: 40px 0;
	background: rgb(79, 182, 206);
	color: white; }

.masthead h2 {
	font-size: 50px;}

.container .masthead h1 {
	margin: 0 auto !important;
	max-width: 90%;
	text-transform: uppercase; }

.container .content {
	background: white;
	padding: 30px 35px; }

.container .content.footer {
  background: none; }

.container .content.footer p {
	margin-bottom: 0;
	color: #888;
	text-align: center;
	font-size: 14px; }

.container .content.footer a {
	color: #888;
	text-decoration: none;
	font-weight: bold; }

.center {
	text-align: center;
}

.content th {
	text-align: left;
	padding-bottom: 10px;
}

.content td {
	max-width: 150px;
	max-height: 150px;
	overflow: hidden;
}

.blue {
	color: rgb(79, 182, 206);
	text-align: center;
}

</style>
</head>
<body>
<table class="body-wrap">
	<tr>
		<td class="container">

			<!-- Message start -->
			<table>
				<tr>
					<td align="center" class="masthead">

						<h1>Gerbang Impian Holdings Sdn Bhd</h1>

					</td>
				</tr>
				<tr>
					<td class="content">

						<p>Dear '.ucwords($firstname.' '.$lastname).',</p>

						<p>Your form has been sent to our beloved staff.<br>
							We will get back to you as soon as possible.<br><br>
						<span class="center">Thank you.</span>

						<h4 class="blue"><u>Enquiry Form</u></h4>
						<table align="center">
							<tr>
								<th>Company Name</th>
								<td>'.$companyname.'</td>
							</tr>
							 <tr>
								 <th>Website Name</th>
								 <td>'.$websitename.'</td>
							 </tr>
							 <tr>
								 <th>Email</th>
								 <td>'.$email.'</td>
							 </tr>
							 <tr>
								 <th>Phone</th>
								 <td>'.$phone.'</td>
							 </tr>
							 <tr>
								 <th>Message</th>
								 <td>'.$message.'</td>
							 </tr>
						 </table>

					</td>
				</tr>
			</table>

		</td>
	</tr>
	<tr>
		<td class="container">

			<!-- Message start -->
			<table>
				<tr>
					<td class="content footer" align="center">
						<p><b>Gerbang Impian Holdings Sdn. Bhd.</b>, <br>No 9-2, Jalan Pusat Perniagaan 2, Pusat Perniagaan <br>Sg.Jelok, 43000, Kajang, Selangor.</p>
						<p>salespayroll@gerbangholdings.com</p>
					</td>
				</tr>
			</table>

		</td>
	</tr>
</table>
</body>';


function sendEmail($to, $subject, $content, $cc){

    // To send HTML mail, the Content-type header must be set
    $headers  = 'MIME-Version: 1.0'."\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";

    // Create email headers
    $headers .= "From: Gerbang Impian Holdings Sdn Bhd <".$to.">\r\n";
    $headers .= "Reply-To: no-reply@gerbangholdings.com\r\n";
    if($cc != ''){ $headers .= "Cc: ".$cc."\r\n"; }

    // Sending email
    if(mail($to, $subject, $content, $headers)){
        $message = 'success';
    } else{
        $message = 'error';
    }
    return $message;
}

$message = sendEmail($to, $subject, $content, $email);



	// date_default_timezone_set("Asia/Kuala_Lumpur");
	// ##Connection
	// $dbhost = 'localhost';
	// $dbuser = 'root';
	// $dbpass = '';
	// $dbName = 'gerbang';
	// $dbConection = mysql_connect($dbhost, $dbuser, $dbpass, true) or die (mysql_error());
	// mysql_select_db($dbName, $dbConection) or die(mysql_error());

	// $company = $_POST['companyname'];
	// $Webname = $_POST['Wname'];
	// $firstname = $_POST['FName'];
	// $lastname = $_POST['Lname'];
	// $email = $_POST['Email'];
	// $phone = $_POST['contact'];
	// $message = $_POST['message'];

    // $sql = "INSERT INTO `details`( `company_name`, `websiteName`, `firstName`, `LastName`, `emails`, `phone`, `message`, `datetime_created`, `datetime_modified`)
			// VALUES ('".mysql_real_escape_string($company)."',
			// '".mysql_real_escape_string($Webname)."',
			// '".mysql_real_escape_string($firstname)."',
			// '".mysql_real_escape_string($lastname)."',
			// '".mysql_real_escape_string($email)."',
			// '".mysql_real_escape_string($phone)."',
			// '".mysql_real_escape_string($message)."',
			// NOW(),
			// NOW() )";
    // $query = mysql_query($sql, $dbConection) or die(mysql_error());

		// if($query == 1){
		// echo "inserted";
	// }
?>
